package com.example.lessontwohomeworktasktwo

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.PhoneNumberUnderscoreSlotsParser
import ru.tinkoff.decoro.watchers.FormatWatcher
import ru.tinkoff.decoro.watchers.MaskFormatWatcher

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val slots = PhoneNumberUnderscoreSlotsParser().parseSlots("+__ (___) ___ __ __")
        val formatWatcher: FormatWatcher = MaskFormatWatcher(MaskImpl.createTerminated(slots))
        formatWatcher.installOn(phone)
        validation.setOnClickListener(View.OnClickListener {
            if (!hasEmptyEditText() && !equalPassAndRepeatPass() && !isValidEmail && !isValidPhone) {
                AlertDialog.Builder(this@MainActivity)
                        .setTitle("Success")
                        .setMessage("Validation was successful")
                        .setPositiveButton("Ok") { dialog, _ -> dialog.dismiss() }.show()
            }
        })
    }

    private fun hasEmptyEditText(): Boolean {
        var hasError = false
        if (login.text.toString().isEmpty()) {
            hasError = true
            login_error.visibility = View.VISIBLE
        } else {
            login_error.visibility = View.GONE
        }
        if (email.text.toString().isEmpty()) {
            hasError = true
            email_error.visibility = View.VISIBLE
        } else {
            email_error.visibility = View.GONE
        }
        if (phone.text.toString().isEmpty()) {
            hasError = true
            phone_error.visibility = View.VISIBLE
        } else {
            phone_error.visibility = View.GONE
        }
        if (pass.text.toString().isEmpty()) {
            hasError = true
            pass_error.visibility = View.VISIBLE
        } else {
            pass_error.visibility = View.GONE
        }
        if (repeat_pass.text.toString().isEmpty()) {
            hasError = true
            repeat_pass_error.visibility = View.VISIBLE
        } else {
            repeat_pass_error.visibility = View.GONE
        }
        return hasError
    }

    private fun equalPassAndRepeatPass(): Boolean {
        var hasError = false
        if (pass.text.toString() != repeat_pass.text.toString()) {
            hasError = true
            AlertDialog.Builder(this@MainActivity)
                    .setTitle("ERROR")
                    .setMessage("Password mismatch")
                    .setPositiveButton("Ok") { dialog, _ -> dialog.dismiss() }.show()
        }
        return hasError
    }

    private val isValidEmail: Boolean
        private get() {
            var hasError = false
            if (!email.text.toString().contains("@") || !email.text.toString().contains(".")) {
                hasError = true
                AlertDialog.Builder(this@MainActivity)
                        .setTitle("ERROR")
                        .setMessage("The e-mail field is entered incorrectly")
                        .setPositiveButton("Ok") { dialog, _ -> dialog.dismiss() }.show()
            }
            return hasError
        }

    private val isValidPhone: Boolean
        private get() {
            var hasError = false
            var s = phone.text.toString()
            s = s.replace("[^0-9]".toRegex(), "")
            val amountOfNumbers = s.split("").toTypedArray()
            if (amountOfNumbers.size != 12) {
                hasError = true
                AlertDialog.Builder(this@MainActivity)
                        .setTitle("ERROR")
                        .setMessage("The phone number field is entered incorrectly")
                        .setPositiveButton("Ok") { dialog, _ -> dialog.dismiss() }.show()
            }
            return hasError
        }
}